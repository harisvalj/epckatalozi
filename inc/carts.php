<?php 
include_once('conn.php');
session_start();
$user_session = $_SESSION['userCart'];
$ukupno = array(); 
$sql1="SELECT * FROM artikli INNER JOIN korpa ON korpa.ArtikalID = artikli.ID WHERE korpa.usersesion LIKE '%$user_session' ";
$result=$conn->query($sql1);
while ($row = $result->fetch_assoc()) {
$artikalID= $row['ArtikalID'];
$naziv= $row['Naziv'];
$kategorija = $row['Kategorija'];
$cijena = $row['Cijena'];
$slika = $row['Slika'];
$kolicina = $row['Kolicina'];

if($kategorija==1){
$KategorijaN = 'Teretni program';
}else if($kategorija==2){
$KategorijaN = 'Putnički program';
}else if($kategorija==3){
$KategorijaN = 'Servisna uputstva';
}else if($kategorija==4){
$KategorijaN = 'Viljuškari';
}else if($kategorija==5){
$KategorijaN = 'Dijagnostički uređaji';
}else{
$KategorijaN = 'Nekategorisano';
}?>
	<tr id="<?php echo $artikalID; ?>">
	<td></td>
		<td>
		<a class="hidden-xs" href="product.php"><img  src="images/artikli/<?php echo $slika; ?>" alt="" class="img-responsive"/></a>
		</td>
		<td><a href="product.php"><?php echo $naziv; ?></a><br/><span><?php echo $KategorijaN; ?></span></td>

		<td class="price" data-cijena="<?php echo $cijena ?>"><?php echo $cijena; ?> KM</td>
		<td><a href="#" id="obrisi" onclick="" data-idb="<?php echo $artikalID; ?>"><i class="fa fa-times-circle fa-2x"></i></a></td>
	</tr>

<?php 

array_push($ukupno, $cijena);
} ?>
<script type="text/javascript">
	$('#kart tr td #obrisi').each(function(){
		$(this).click(function(e){
		e.preventDefault();
		var sum=0;
		var delcart = $(this).attr("data-idb");
		$.ajax({
				type: 'get',
				url: 'obrisicart.php',
				data: 'idb=' + delcart ,
				success: function(result) {
					$('#'+delcart).remove();
					$('.popcart').hide('fast', function() {
			  		});
					var rowCount = $('#kart tr').length;
					$('span.sub-tot span.brojka').html(rowCount);
					$.post( "inc/carts.php", function( data ) {
						$( "#kart" ).html( data );
					});
					$( "#kart" ).html( data );
  				var rowCount = $('#kart tr').length;
				$('span.sub-tot span.brojka').html(rowCount);
				$('.price').each(function(){
					var broj=$(this).attr("data-cijena");
					sum += Number(broj);
    			});
				$('.popcart .popcart-tot > p > span').text(sum+' KM');
					
					$('.popcart').show('fast', function() {
			  		});
				},
				error: function(){
					alert('Dogodila se pogreška, pokušajte ponovo!');
				}
			});	
		});
	});
</script>