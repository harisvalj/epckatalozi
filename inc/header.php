<div class="header"><!--Header -->
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-md-4 main-logo">
					<a href="index.php"><img src="images/logo.png" alt="logo" class="logo img-responsive" /></a>
				</div>
				<div class="col-md-5 col-md-offset-3" >
						<div class="top" style="float:left;">
							<!--<a href="#" id="reg" class="btn btn-default btn-dark" onclick="alert('Stranica je još izradi i direktno naručivanje nije omogućeno zbog lijenosti našeg programera. narudžbu vršite isključivo preko maila info@epckatalozi.com (otvaramo često mail) ili direktno na broj telefona.');">Online pristup</a>-->
							<!--<div class="regwrap">
								<div class="row">
									<div class="col-md-4 regform">
										<div class="title-widget-bg">
											<div class="title-widget">Login</div>
										</div>
										<form role="form">
											<div class="form-group">
												<input type="text" class="form-control" id="username" placeholder="Username">
											</div>
											<div class="form-group">
												<input type="password" class="form-control" id="password" placeholder="password">
											</div>
											<div class="form-group">
												<button class="btn btn-default btn-red btn-sm">Sign In</button>
											</div>
										</form>
									</div>
									<div class="col-md-4">
										<div class="title-widget-bg">
											<div class="title-widget">Register</div>
										</div>
										<p>
											New User? By creating an account you be able to shop faster, be up to date on an order's status...
										</p>
										<button class="btn btn-default btn-yellow">Register Now</button>
									</div>
								</div>
							</div>-->
						</div>
						<div class="col-md-3 col-md-offset-3 machart" style="">
						<button id="popcart" class="btn btn-default btn-chart btn-sm " onclick="//alert('Stranica je još izradi i direktno naručivanje nije omogućeno zbog lijenosti našeg programera. narudžbu vršite isključivo preko maila info@epckatalozi.com (otvaramo često mail) ili direktno na broj telefona.');"><span class="mychart">Predračun</span></button>
						<div class="popcart" >
							<table class="table table-condensed popcart-inner">
								<tbody id="kart">
									
								</tbody>
							</table>
							<span class="sub-tot">Ukupno artikala : <span class="brojka"></span> 
							<br/>
							<div class="btn-popcart">
								<a href="showcart.php" class="btn btn-default btn-success btn-sm">Predračun</a>
							</div>
							<!--<div class="btn-popcart" style="margin-left:10px;">
								<a href="#" id="osvjezi" class="btn btn-default btn-info btn-sm">Osvježi</a>
							</div>-->
							<div class="popcart-tot">
								<p>
									Ukupno<br/>
									<span>
									<?php 
										$arrlength = count($ukupno);
										$suma=0;
										for($x = 0; $x < $arrlength; $x++) {
											$suma =  $suma + $ukupno[$x];										    
										}
										echo $suma.' KM';
										$_SESSION['suma']=$suma;
									?>
									</span>
								</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="dashed"></div>
	</div><!--Header -->
	<div class="main-nav"><!--end main-nav -->
		<div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>	
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
							<?php 
							error_reporting(0);			
								$kate=$_GET['kat'];
								if($kate>0){
									$brojkastr=$kate;
								}else{
									$brojkastr=$_GET['ID'];
								}
								if(!isset($brojkastr)){
							?>
								<li><a href="index.php" class="active">Početna</a><div class="curve"></div></li>
								<?php }else{?>
								<li><a href="index.php" >Početna</a></li>
								<?php }?>
							<?php	
								
								$sql1="SELECT * FROM kategorije ";
								$rez=$conn->query($sql1);
								while ($row = $rez->fetch_assoc()) {
									$ID=$row['ID'];
									$Ime = $row['Naziv'];

								?>
								<?php if($brojkastr==$ID){ $klasa='active';}else{ $klasa='';}?>
								<li><a href="kategorija.php?ID=<?php echo $ID; ?>" class="dropdown-toggle <?php echo $klasa; ?>"><?php echo $Ime; ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div><!--end main-nav -->
	