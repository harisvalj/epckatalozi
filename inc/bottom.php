<div class="f-widget"><!--footer Widget-->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4"><!--footer newsletter widget-->

				</div><!--footer newsletter widget-->
				<div class="col-md-4"><!--footer contact widget-->
					<div class="title-widget-bg">
						<div class="title-widget-cursive">Kontakt</div>
					</div>
					<ul class="contact-widget">
						<li class="fmobile"><a href="tel:+38762941029">+387 62 941 029</a><br /><i>Dostupan i na viber</i></li>
						<li class="fmail lastone"><a href="mailto:info@epckatalozi.com">info@epckatalozi.com</a><br><i>Napomena: Redovno se provjerava mail.</i></li>
					</ul>
				</div><!--footer contact widget-->
			</div>
			<div class="spacer"></div>
		</div>
	</div><!--footer Widget-->