<?php 
session_start();
include_once('inc/conn.php'); 

if(!isset($_SESSION['userCart'])){
$_SESSION['userCart'] = md5(uniqid(rand(), true));
}
$user_session = $_SESSION['userCart'];
?>
<?php 
	$ID=$_GET['ID'];
	if(isset($ID)){
		$sql1="SELECT * FROM artikli WHERE ID = '$ID' ";
							$rez=$conn->query($sql1);
							while ($row = $rez->fetch_assoc()) {
								$naslov=$row['Naziv'];
								$Opis = $row['Opis'];
								$Cijena = $row['Cijena'];
								$Cijena2 = $row['CijenaOld'];
								$slika = $row['Slika'];
								$istaknuto = $row['Istaknuto'];
								$Kategorija = $row['Kategorija'];
								$proizvodac = $row['proizvodac'];
								$verzija = $row['verzija'];								
								$godina = $row['godina'];
							}
							if($Kategorija==1){
							$KategorijaN = 'Teretni program';
							}else if($Kategorija==2){
							$KategorijaN = 'Putnički program';
							}else if($Kategorija==3){
							$KategorijaN = 'Servisna uputstva';
							}else if($Kategorija==4){
							$KategorijaN = 'Viljuškari';
							}else if($Kategorija==5){
							$KategorijaN = 'Dijagnostički uređaji';
							}else{
							$KategorijaN = 'Nekategorisano';
							}
							if($Cijena2!=$Cijena){ $rasprodaja="on-sale";}else{$rasprodaja="";}
			}else{ header("location: 404.php"); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title><?php echo $naslov; ?></title>
	
	<link rel="shortcut icon" href="images/fav.png">
	
    <!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='font-awesome/css/font-awesome.css' rel="stylesheet" type="text/css">
	<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css" />
	<!-- fancy Style -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="js/product/jquery.fancybox.css¿v=2.1.5.css" media="screen" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<style type="text/css">
		ul.gallery li { display: inline; margin-bottom: 20px;}
		ul.gallery{
		    width: 100%;
		    text-align: center;
		}
		ul.gallery h1{
			padding-bottom: 10px;
		}

		ul.gallery h1::after{
			content: "";
			display: block;
			margin-top: 5px;
			border-bottom: 1px solid #ccc;

		}
		ul.gallery img{
			margin-bottom: 25px;
			margin-left: 10px;
			margin-right: 10px;
		}
	</style>
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="../../../oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="../../../oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>


<!-- Copyright © 2008. Spidersoft Ltd -->
<style>
A.applink:hover {border: 2px dotted #DCE6F4;padding:2px;background-color:#ffff00;color:green;text-decoration:none}
A.applink       {border: 2px dotted #DCE6F4;padding:2px;color:#2F5BFF;background:transparent;text-decoration:none}
A.info          {color:#2F5BFF;background:transparent;text-decoration:none}
A.info:hover    {color:green;background:transparent;text-decoration:underline}
</style>


	<div id="wrapper">
	<?php include_once('inc/header.php'); ?>	
	<div class="container">
		
		<div class="row">
		<div class="col-md-3"><!--sidebar-->
				<div class="title-bg">
					<div class="title">Kategorije</div>
				</div>
				
				<div class="categorybox">
					<ul>
							
							<?php	
								$sql1="SELECT * FROM kategorije";
								$rez=$conn->query($sql1);
								while ($row = $rez->fetch_assoc()) {
									$ID=$row['ID'];
									$Ime = $row['Naziv'];
								?>
								<li><a href="kategorija.php?ID=<?php echo $ID; ?>" class="dropdown-toggle"><?php echo $Ime; ?></a></li>
								<?php } ?>
					</ul>
				</div>
				
				
				
			</div><!--sidebar-->
			<div class="col-md-9"><!--Main content-->
				<div class="title-bg">
					<div class="title"><?php echo $naslov; ?></div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="dt-img">
						<div class="detpricetag <?php echo $rasprodaja; ?>"><div class="inner <?php echo $rasprodaja; ?>"><span class="onsale"><?php if($Cijena2!=$Cijena){?><span class="oldprice"><?php echo $Cijena2; ?> KM</span><?php }?><?php echo $Cijena; ?> KM</span></div></div>
							<a class="fancybox" href="images/artikli/<?php echo $slika; ?>" data-fancybox-group="gallery" title="<?php echo $naslov; ?>"><?php if($istaknuto == 1 ){ ?><img src="images/istaknuto.png" style="border: none; position: absolute; top: 0;"><?php }?><img src="images/artikli/<?php echo $slika; ?>" alt="" class="img-responsive" /></a> </div>
					</div>
					<div class="col-md-6 det-desc">
						<div class="productdata">
							<div class="infospan">Proizvođač <span><?php echo $proizvodac; ?></span></div>
							<div class="infospan">Verzija <span><?php echo $verzija; ?></span></div>
							<div class="infospan">Godina <span><?php echo $godina; ?></span></div>							
							<form class="form-horizontal ava" role="form">
								<div class="form-group">
									<label for="qty" class="col-sm-2 control-label">Količina</label>
									<div class="col-sm-3">
										<select class="form-control" id="qty">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="col-md-5">
										<a href="#" class="add-to-cart" data-id="<?php echo $ID ?>">Dodaj u predračun</a>
									</div>
									<div class="clearfix"></div>
								</div>
							</form>							
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="tab-review">
					<ul id="myTab" class="nav nav-tabs shop-tab">
						<li class="active"><a href="#desc" data-toggle="tab">Opis</a></li>
					</ul>
					<div id="myTabContent" class="tab-content shop-tab-ct">
						<div class="tab-pane fade active in" id="desc">
							<p>
							<?php echo $Opis; ?>
							</p>
						</div>
						<ul class="gallery clearfix">
								<h1>Galerija</h1>
								<li><a href="images/artikli/MERCEDES03.png?lol=lol" rel="prettyPhoto[gallery1]" title="You can add caption to pictures. You can add caption to pictures. You can add caption to pictures."><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Red round shape" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Fire!" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Rock climbing" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Fly kite, fly!" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>
								<li><a href="images/artikli/MERCEDES03.png" rel="prettyPhoto[gallery1]"><img src="images/artikli/MERCEDES03.png" width="125" height="125" alt="Nice building" /></a></li>

							</ul>
						<div class="tab-pane fade" id="rev">
							<p class="dash">
							<span>Jhon Doe</span> (11/25/2012)<br/><br/>
							Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.
							</p>
							<h4>Write Review</h4>
							<form role="form">
							<div class="form-group">
								<input type="text" class="form-control" id="name" >
							</div>
							<div class="form-group">
								<textarea class="form-control" id="text" ></textarea>
							</div>
							<div class="form-group">
								<div class="rate"><span>Rating:</span></div>
								<div class="starwrap">
									<div id="default"></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<button type="submit" class="btn btn-default btn-red btn-sm">Submit</button>
						</form>
							
						</div>
					</div>
				</div>
				
				<div id="title-bg">
					<div class="title">Slični proizvodi</div>
				</div>
				<div class="row prdct"><!--Products-->
				<?php
						$sql1="SELECT * FROM artikli WHERE Kategorija = '$Kategorija' ORDER BY rand() LIMIT 4 ";
						$rez=$conn->query($sql1);
						while ($row = $rez->fetch_assoc()) {
							$id= $row["ID"];	
							$Naziv= $row["Naziv"];
							$Cijena= $row["Cijena"];							
							$Cijena2= $row["CijenaOld"];
							$slika= $row["Slika"];
							$Kategorija = $row["Kategorija"];
							if($Kategorija==1){
							$KategorijaN = 'Teretni program';
							}else if($Kategorija==2){
							$KategorijaN = 'Putnički program';
							}else if($Kategorija==3){
							$KategorijaN = 'Servisna uputstva';
							}else if($Kategorija==4){
							$KategorijaN = 'Viljuškari';
							}else if($Kategorija==5){
							$KategorijaN = 'Dijagnostički uređaji';
							}else{
							$KategorijaN = 'Nekategorisano';
							}
							if($Cijena2!=$Cijena){ $rasprodaja="on-sale";}else{$rasprodaja="";}
							?>	
						<div class="col-md-3">
						<div class="productwrap">
							<div class="pr-img">
								<!--<div class="hot"></div>-->
								<a class="hidden-xs" href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><img src="images/artikli/<?php echo $slika; ?>" alt="" class="img-responsive"/></a>
							</div>
							<div class="row svezajedno">
								<div class="col-md-8">
									<span class="smalltitle"><a href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><?php echo $Naziv; ?></a></span>
									<div class="ispod">
										<span class="smalldesc"><?php echo $KategorijaN; ?></span><br/>
										<span class="smalldesc"><a href="#" class="add-to-cart" data-id="<?php echo $id ?>">Dodaj u predračun</a></span>
									</div>
								</div>
								<div class="col-md-4 cijena">
								<div class="pricetag <?php echo $rasprodaja; ?>" style="margin-right: 10px;"><div class="inner <?php echo $rasprodaja; ?>"><span class="onsale"><?php if($Cijena2!=$Cijena){?><span class="oldprice"><?php echo $Cijena2; ?> KM</span><?php }?><?php echo $Cijena; ?> KM</span></div></div>								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div><!--Products-->
				<div class="spacer"></div>
			<!--Main content-->
			</div>
		</div>
	</div>
	
	<?php include_once('inc/bottom.php'); ?>    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- map -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
	<script type="text/javascript" src="js/jquery.ui.map.js"></script>
	<script type="text/javascript" src="js/demo.js"></script>
	
	<!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>
	
	<!-- rating -->
	<script src="js/rate/jquery.raty.js"></script>
	<script src="js/labs.js" type="text/javascript"></script>
	
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/product/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	
	<!-- fancybox -->
    <script type="text/javascript" src="js/product/jquery.fancybox.jsv=2.1.5"></script>
	
	<!-- custom js -->
    <script src="js/shop.js"></script>
    <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$(".gallery a[rel^='prettyPhoto']").prettyPhoto();
				
			

			});
			</script>
	</div>
  </body>
</html>
