<?php 
session_start();
include_once('inc/conn.php'); 

if(!isset($_SESSION['userCart'])){
$_SESSION['userCart'] = md5(uniqid(rand(), true));
}
$user_session = $_SESSION['userCart'];
$kat = $_GET['ID'];
if($kat>=6){
	header("Location: index.php");
}else{
$sql1="SELECT * FROM kategorije WHERE ID = '$kat'";
									$rez=$conn->query($sql1);
									while ($row = $rez->fetch_assoc()) {
										$Naslov=$row['Naziv'];
									}
}
?>

<!DOCTYPE html>
<html lang="bs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $Naslov; ?></title>

    <!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css" />
	
	<!-- owl Style -->
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/owl.transitions.css" />
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="../../../oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="../../../oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>


<!-- Copyright © 2008. Spidersoft Ltd -->
<style>
A.applink:hover {border: 2px dotted #DCE6F4;padding:2px;background-color:#ffff00;color:green;text-decoration:none}
A.applink       {border: 2px dotted #DCE6F4;padding:2px;color:#2F5BFF;background:transparent;text-decoration:none}
A.info          {color:#2F5BFF;background:transparent;text-decoration:none}
A.info:hover    {color:green;background:transparent;text-decoration:underline}
</style>
<!-- /Copyright © 2008. Spidersoft Ltd -->


  <div id="wrapper">
	<?php include_once('inc/header.php'); ?>	
	
	<div class="container">
		<div class="row">
			<div class="col-md-3"><!--sidebar-->
				<div class="title-bg">
					<div class="title">Kategorije</div>
				</div>
				
				<div class="categorybox">
					<ul>
								
								<?php	
									$sql1="SELECT * FROM kategorije";
									$rez=$conn->query($sql1);
									while ($row = $rez->fetch_assoc()) {
										$ID=$row['ID'];
										$Ime = $row['Naziv'];
									?>
									<li><a href="kategorija.php?ID=<?php echo $ID; ?>" class="dropdown-toggle"><?php echo $Ime; ?></a></li>
									<?php } ?>
						</ul>
				</div>
			
				
			</div><!--sidebar-->
			<div class="col-md-9"><!--Main content-->
				

				<div class="title-bg">
					<div class="title">Posljednji proizvodi</div>
				</div>
				<div class="row prdct">
					<?php
						$sql1="SELECT * FROM artikli WHERE Kategorija = '$kat'";
						$rez=$conn->query($sql1);
						while ($row = $rez->fetch_assoc()) {
							$id= $row["ID"];	
							$Naziv= $row["Naziv"];
							$Cijena= $row["Cijena"];							
							$Cijena2= $row["CijenaOld"];
							$slika= $row["Slika"];
							$Kategorija = $row["Kategorija"];
							if($Kategorija==1){
							$KategorijaN = 'Teretni program';
							}else if($Kategorija==2){
							$KategorijaN = 'Putnički program';
							}else if($Kategorija==3){
							$KategorijaN = 'Servisna uputstva';
							}else if($Kategorija==4){
							$KategorijaN = 'Viljuškari';
							}else if($Kategorija==5){
							$KategorijaN = 'Dijagnostički uređaji';
							}else{
							$KategorijaN = 'Nekategorisano';
							}
							if($Cijena2!=$Cijena){ $rasprodaja="on-sale";}else{$rasprodaja="";}
							?>							
							<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="productwrap">
							<div class="pr-img">
								<!--<div class="hot"></div>-->
								<a class="hidden-xs" href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><img src="images/artikli/thumb/<?php echo $slika; ?>" alt="" class="img-responsive"/></a>
							</div>
							<div class="row svezajedno">
								<div class="col-md-8">
									<span class="smalltitle"><a href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><?php echo $Naziv; ?></a></span>
									<div class="ispod">
										<span class="smalldesc"><?php echo $KategorijaN; ?></span><br/>
										<span class="smalldesc"><a href="#" class="add-to-cart" data-id="<?php echo $id ?>">Dodaj u predračun</a></span>
									</div>
								</div>
								<div class="col-md-4 cijena">
								<div class="pricetag <?php echo $rasprodaja; ?>" style="margin-right: 10px;" ><div class="inner <?php echo $rasprodaja; ?>"><span class="onsale"><?php if($Cijena2!=$Cijena){?><span class="oldprice"><?php echo $Cijena2; ?> KM</span><?php }?><?php echo $Cijena; ?> KM</span></div></div>								</div>
							</div>
						</div>
					</div>
							<?php

						}
					
					?>
					</div>
				</div><!--Products-->
				<div class="spacer"></div>
			</div><!--Main content-->
		</div>
	</div>
	
	<div class="f-widget"><!--footer Widget-->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4"><!--footer newsletter widget-->

				</div><!--footer newsletter widget-->
				<div class="col-md-4"><!--footer contact widget-->
					<div class="title-widget-bg">
						<div class="title-widget-cursive">Kontakt</div>
					</div>
					<ul class="contact-widget">
						<li class="fmobile"><a href="tel:+38762941029">+387 62 941 029</a><br /><i>Dostupan i na viber</i></li>
						<li class="fmail lastone"><a href="mailto:info@epckatalozi.com">info@epckatalozi.com</a><br><i>Napomena: Redovno se provjerava mail.</i></li>
					</ul>
				</div><!--footer contact widget-->
			</div>
			<div class="spacer"></div>
		</div>
	</div><!--footer Widget-->
	
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- map -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
	<script type="text/javascript" src="js/jquery.ui.map.js"></script>
	<script type="text/javascript" src="js/demo.js"></script>
	
	<!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>
	
	<!-- rating -->
	<script src="js/rate/jquery.raty.js"></script>
	<script src="js/labs.js" type="text/javascript"></script>
	
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/product/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	
	<!-- fancybox -->
    <script type="text/javascript" src="js/product/jquery.fancybox.js=2.1.5"></script>
	
	<!-- custom js -->
    <script src="js/shop.js"></script>
	</div>
  </body>
</html>
