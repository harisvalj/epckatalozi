<?php 
	session_start();
	include_once('../inc/conn.php');
	if(isset($_SESSION['prijavljen'])){
	 ?>
	}
<!doctype html>

<html class="fixed">
	
<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.5.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 Nov 2015 11:31:15 GMT -->
<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Admin Panel</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />

		
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Specific Page Vendor CSS -->		
		<link rel="stylesheet" href="assets/vendor/jquery-ui/jquery-ui.css" />		
		<link rel="stylesheet" href="assets/vendor/jquery-ui/jquery-ui.theme.css" />		
		<link rel="stylesheet" href="assets/vendor/select2/css/select2.css" />		
		<link rel="stylesheet" href="assets/vendor/select2-bootstrap-theme/select2-bootstrap.css" />		
		<link rel="stylesheet" href="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />		
		<link rel="stylesheet" href="assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />		
		<link rel="stylesheet" href="assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />		
		<link rel="stylesheet" href="assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />		
		<link rel="stylesheet" href="assets/vendor/dropzone/basic.css" />		
		<link rel="stylesheet" href="assets/vendor/dropzone/dropzone.css" />		
		<link rel="stylesheet" href="assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />		
		<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />		
		<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />		
		<link rel="stylesheet" href="assets/vendor/codemirror/lib/codemirror.css" />		
		<link rel="stylesheet" href="assets/vendor/codemirror/theme/monokai.css" />
		<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />		
		<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />			
		<!-- Theme CSS -->
		
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Theme Custom CSS -->
		
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		
		<script src="assets/vendor/modernizr/modernizr.js"></script>		
		<script src="assets/vendor/style-switcher/style.switcher.localstorage.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="../index.php" class="logo">
						<img src="assets/images/logo.png" height="35" alt="Porto Admin" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="assets/images/%21logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/%21logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<span class="name"><?php echo $_SESSION['username']; ?></span>
								<span class="role">administrator</span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-signin.html"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<?php include_once('meni.php'); ?>
							</nav>
						</div>
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Dashboard</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								 
								<li><span>Dashboard</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<div class="pozadina" style="background-color:#fff;">
						<div class="col-md-12" style="margin-bottom:20px;">
							<div class="col-md-6">
								<form class="form-horizontal form-bordered" method="POST" action="addkat.php" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-md-4 control-label" for="inputDefault">Dodaj kategoriju</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="inputDefault" name="kategorija">
										</div>
									</div>
							</div>
							<div class="col-md-6">
								<input type="submit" class="btn btn-info">
							</div>

								</form>
						</div>
					<!-- start: page -->
					<?php 
						$sql3="SELECT * FROM kategorije";
						$rez3=$conn->query($sql3);
						while ($row2 = $rez3->fetch_assoc()) {
							$KatID=$row2['ID'];
							$KatNaziv=$row2['Naziv'];
						?>
						<div class="col-md-6">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
									</div>

									<h2 class="panel-title">Lista <?php echo $KatNaziv; ?> </h2>
									<p class="panel-subtitle">Lista dodanih artikala</p>
								</header>
								<div class="panel-body">
									<ul>
									<?php	
													$sql1="SELECT * FROM artikli WHERE Kategorija = '$KatID' ORDER BY ID DESC ";
													$rez=$conn->query($sql1);
													while ($row = $rez->fetch_assoc()) {
														$ID=$row['ID'];
														$Naslov=$row['Naziv'];
														$Opis = $row['Opis'];
														$Cijena = $row['Cijena'];
														$Cijena2 = $row['CijenaOld'];
														$slika = $row['Slika'];
														$istaknuto = $row['Istaknuto'];
														$Kategorija = $row['Kategorija'];?>
														<li><a href="../product.php?ID=<?php echo $ID; ?>" class="text-info"><?php echo $Naslov; ?></a> | <a href="uredi.php?ID=<?php echo $ID; ?>" class="btn btn-info btn-xs" >Uredi artikal</a>  | <a href="obrisi.php?ID=<?php echo $ID; ?>" class="btn btn-danger btn-xs" >Obriši artikal</a></li>
														
														

													<?php } ?>
									</ul>
								</div>
							</section>
						</div>
						<?php } ?>

					<!-- end: page -->

					</div>
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
			
							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="assets/images/%21sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="assets/images/%21sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="assets/images/%21sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="assets/images/%21sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		
		<script src="assets/vendor/jquery/jquery.js"></script>		
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="assets/vendor/jquery-cookie/jquery-cookie.js"></script>		
		<script src="assets/vendor/style-switcher/style.switcher.js"></script>		
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>		
		<script src="assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="assets/vendor/jquery-ui/jquery-ui.js"></script>		
		<script src="assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>		
		<script src="assets/vendor/jquery-appear/jquery-appear.js"></script>		
		<script src="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>		
		<script src="assets/vendor/flot/jquery.flot.js"></script>		
		<script src="assets/vendor/flot.tooltip/flot.tooltip.js"></script>		
		<script src="assets/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="assets/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="assets/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="assets/vendor/jquery-sparkline/jquery-sparkline.js"></script>		
		<script src="assets/vendor/raphael/raphael.js"></script>		
		<script src="assets/vendor/morris.js/morris.js"></script>		
		<script src="assets/vendor/gauge/gauge.js"></script>		
		<script src="assets/vendor/snap.svg/snap.svg.js"></script>		
		<script src="assets/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="assets/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>		
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>		
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>		
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>		
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>		
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		<script src="assets/vendor/summernote/summernote.js"></script>		
		<script src="script.js"></script>		
		<!-- Theme Base, Components and Settings -->
		<script type="text/javascript">
$(document).ready(function() {
	$('#summernote').summernote({
		height: "500px"
	});
});
		</script>
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		
		<script src="assets/javascripts/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->		
		<script>		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)		  })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');		  ga('create', 'UA-42715764-8', 'auto');		  ga('send', 'pageview');		</script>
		<!-- Examples -->
		
		<script src="assets/javascripts/dashboard/examples.dashboard.js"></script>
	</body>
<?php }else{header("Location: prijava.php"); } ?>
</html>