<?php 
session_start();
include_once('inc/conn.php'); 
$user_session = $_SESSION['userCart'];

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>Predračun</title>
	
	<link rel="shortcut icon" href="images/fav.png">
	
    <!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css" />
	<!-- fancy Style -->
		<style type="text/css">
		@media screen and (max-width: 769px) {
    		table{
				width: 100% !important;
			}
			.chart td{
				width: 33% !important;
			}
			}

			
			.chart td, .chart td span, .chart p{
				font-size: 18px !important;
			    text-align: center ;
			    font-weight: 400 !important;
			    color: #767272 !important;
			    border-color: #d5dade !important;
		</style>	
		

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="../../../oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="../../../oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>





	<div id="wrapper">
	
	<?php include_once('inc/header.php'); ?>

	<div class="container">
		<div class="title-bg">
			<div class="title">Predračun</div>
		</div>
		
		<div class="table-responsive">
			<table class="table table-bordered chart">
				<thead>
					<tr>
						<th>Slika</th>
						<th>Naziv</th>
						<th>Cijena</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$ukupno = array();
					$narudzba = array();
					$sql1="SELECT artikli.ID, artikli.Naziv, artikli.Opis, artikli.Cijena, artikli.CijenaOld, artikli.Slika, artikli.Istaknuto, artikli.Kategorija, artikli.proizvodac, artikli.godina, artikli.verzija FROM artikli RIGHT JOIN korpa ON korpa.ArtikalID = artikli.ID
 WHERE usersesion = '$user_session' ";
					$rez=$conn->query($sql1);
						while ($row = $rez->fetch_assoc()) {
							$ID=$row['ID'];	
							$Naziv=$row['Naziv'];
							$Opis=$row['Opis'];
							$Cijena=$row['Cijena'];
							$CijenaOld=$row['CijenaOld'];
							$Slika=$row['Slika'];
							$Istaknuto=$row['Istaknuto'];
							$Kategorija=$row['Kategorija'];
							$proizvodac=$row['proizvodac'];
							$godina=$row['godina'];
							$verzija=$row['verzija'];
							if($Cijena != $CijenaOld){
								$CijenaNew = $CijenaOld;
							}else{$CijenaNew = $Cijena;}
							if($kategorija==1){
								$KategorijaN = 'Teretni program';
								}else if($kategorija==2){
								$KategorijaN = 'Putnički program';
								}else if($kategorija==3){
								$KategorijaN = 'Servisna uputstva';
								}else if($kategorija==4){
								$KategorijaN = 'Viljuškari';
								}else if($kategorija==5){
								$KategorijaN = 'Dijagnostički uređaji';
								}else{
								$KategorijaN = 'Nekategorisano';
								}
							array_push($ukupno, $CijenaNew);
							$Opis1=substr($Opis, 0,150);
							array_push($narudzba, $Naziv);
				?>
					<tr>
						<td><img src="images/artikli/<?php echo $Slika; ?>" width="100" alt="" /></td>
						<td><?php echo $Naziv; ?></td>
						<td><?php echo $CijenaNew; ?> KM</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row">

				<form action="slpred.php" method="post">
			<div class="col-md-6">
				<form class="form-horizontal coupon" role="form">
					<div class="form-group">
						<label for="coupon" class="col-sm-3 control-label">E-mail</label>
						<div class="col-sm-8">
							<input type="email" name="Email" class="form-control" id="mail" placeholder="Unesite Vaš E-mail">
							<p class="error" style="display:none; color:red; margin:5px;">Upišite mail</p>
						</div> 
					</div> 
					<div class="form-group">
						<label for="coupon" class="col-sm-3 control-label">Telefon</label>
						<div class="col-sm-8">
							<input type="text" name="Telefon" class="form-control" id="Telefon" placeholder="Upišite broj telefona:">
							<p class="error" style="display:none; color:red; margin:5px;">Upišite broj telefona</p>
						</div> 
					</div> 
					<div class="form-group">
						<label for="coupon" class="col-sm-3 control-label">2+2=</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="chapta" placeholder="2+2=4">
							<p class="error1" style="display:none; color:red; margin:5px;">Upišite 4</p>
							<p class="error2" style="display:none; color:red; margin:5px;">Rezultat nije tačan!</p>
						</div>
					</div>
			
			</div>
			<div class="col-md-3 col-md-offset-3">
			<div class="subtotal-wrap">
				<div class="subtotal">
					<p style="display:none;">Ukupno : <?php 
										$arrlength = count($ukupno);
										$suma=0;
										for($x = 0; $x < $arrlength; $x++) {
											$suma =  $suma + $ukupno[$x];										    
										}
										echo $suma.' KM';
										$_SESSION['suma']=$suma;
									?></p>
					<p style="display:none;">PDV 17% : <?php echo $sve=0; ?> KM</p>
				</div>
				<div class="total">Ukupno : <span class="bigprice"><?php echo $sve+$suma; ?> KM</span></div>
				<div class="clearfix"></div>
					<input type="submit" id="predracun" class="btn btn-default btn-yellow" value="Pošalji predračun"><br/>	
				<p style="color: #a9a9a9; font-size: 12px; font-style: italic; text-align: right;">Predračun se šalje na mail, te nakon toga će Vas kontaktirati!</p>
				
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
		<div class="spacer"></div>

				</form>
	</div>
	
	<?php include_once('inc/bottom.php'); ?>
	
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- map -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
	<script type="text/javascript" src="js/jquery.ui.map.js"></script>
	<script type="text/javascript" src="js/demo.js"></script>
	
	<!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>
	
	<!-- rating -->
	<script src="js/rate/jquery.raty.js"></script>
	<script src="js/labs.js" type="text/javascript"></script>
	
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/product/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	
	
	<!-- custom js -->
    <script src="js/shop.js"></script>
    <script type="text/javascript">
			/*$('#predracun').click(function(){
				var mail = $('#mail').val();
				var chapta = $('#chapta').val();
				if(mail === ''){
					$('.error').show();
				}else{
					if(chapta===''){
						$('.error1').show();
					}else if(chapta === '4'){
						$('tbody tr').each(function(){
							alert('koliko puta');
						});

					}else{
						$('.error2').show();
					}
				}
			});*/
		</script>
	</div>
  </body>
</html>
