<?php 
session_start();
include_once('inc/conn.php'); 

if(!isset($_SESSION['userCart'])){
$_SESSION['userCart'] = md5(uniqid(rand(), true));
}
$user_session = $_SESSION['userCart'];

?>
<!DOCTYPE html>
<html lang="bs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="3I1OQ-fokiJ9Mm7NwqRUFsf5jQ9uK9sPG5-KkcTCtUE" />
    <title><?php echo $naziv; ?></title>

    <!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Main Style -->
	
	<!-- owl Style -->
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/owl.transitions.css" />
	<link rel="stylesheet" href="style.css" />
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="../../../oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="../../../oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>


<!-- Copyright © 2008. Spidersoft Ltd -->
<style>
A.applink:hover {border: 2px dotted #DCE6F4;padding:2px;background-color:#ffff00;color:green;text-decoration:none}
A.applink       {border: 2px dotted #DCE6F4;padding:2px;color:#2F5BFF;background:transparent;text-decoration:none}
A.info          {color:#2F5BFF;background:transparent;text-decoration:none}
A.info:hover    {color:green;background:transparent;text-decoration:underline}
</style>
<!-- /Copyright © 2008. Spidersoft Ltd -->


  <div id="wrapper">
	<?php include_once('inc/header.php'); ?>
	

		
		<div class="f-widget featpro">
		<div class="container">
			<div class="title-widget-bg">
				<div class="title-widget">Istaknuti proizvodi</div>
				<div class="carousel-nav">
					<a class="prev"></a>
					<a class="next"></a>
				</div>
			</div>
			<div id="product-carousel" class="owl-carousel owl-theme">
				<?php
					$sql1="SELECT * FROM artikli WHERE Istaknuto = 1 ORDER BY ID DESC LIMIT 7";
						$rez=$conn->query($sql1);
						while ($row = $rez->fetch_assoc()) {
							$id= $row["ID"];	
							$Naziv= $row["Naziv"];
							$Cijena= $row["Cijena"];							
							$Cijena2= $row["CijenaOld"];
							$slika= $row["Slika"];
							$Kategorija = $row["Kategorija"];
						if($Kategorija==1){
						$KategorijaN = 'Teretni program';
						}else if($Kategorija==2){
						$KategorijaN = 'Putnički program';
						}else if($Kategorija==3){
						$KategorijaN = 'Servisna uputstva';
						}else if($Kategorija==4){
						$KategorijaN = 'Viljuškari';
						}else if($Kategorija==5){
						$KategorijaN = 'Dijagnostički uređaji';
						}else{
						$KategorijaN = 'Nekategorisano';
						}
						if($Cijena2!=$Cijena){ $rasprodaja="on-sale";}else{$rasprodaja="";}
					?>
					<div class="item">
						<div class="productwrap">
							<div class="pr-img">
								<!--<div class="hot"></div>-->
								<a class="hidden-xs" href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><img src="images/artikli/<?php echo $slika; ?>" alt="" class="img-responsive"/></a>
								<div class="pricetag <?php echo $rasprodaja; ?>"><div class="inner <?php echo $rasprodaja; ?>"><span class="onsale"><?php if($Cijena2!=$Cijena){?><span class="oldprice"><?php echo $Cijena2; ?> KM</span><?php }?><?php echo $Cijena; ?> KM</span></div></div>
							</div>
								<span class="smalltitle"><a href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><?php echo $Naziv; ?></a></span>
								<span class="smalldesc"><?php echo $KategorijaN; ?></span><br/>
								<span class="smalldesc"><a href="#" class="add-to-cart" data-id="<?php echo $id ?>">Dodaj u predračun</a></span>
						</div>
					</div>
					<?php }?>
			</div>
		</div>
	</div>
	
	
	
	<div class="container">
		<div class="row">
			<div class="col-md-3"><!--sidebar-->
				<div class="title-bg">
					<div class="title">Kategorije</div>
				</div>
				
				<div class="categorybox">
					<ul>
							
							<?php	
								$sql1="SELECT * FROM kategorije";
								$rez=$conn->query($sql1);
								while ($row = $rez->fetch_assoc()) {
									$ID=$row['ID'];
									$Ime = $row['Naziv'];
								?>
								<li><a href="kategorija.php?ID=<?php echo $ID; ?>" class="dropdown-toggle"><?php echo $Ime; ?></a></li>
								<?php } ?>
					</ul>
				</div>
				
				<div class="ads">
					<a href="product.php"><img src="images/ads.png" class="img-responsive" alt="" /></a>
				</div>
				
			</div><!--sidebar-->
			<div class="col-md-9"><!--Main content-->
				<div class="title-bg">
					<div class="title"><?php echo $aboutnas; ?></div>
				</div>
				<p><?php echo $abouttext; ?></p>

				<div class="title-bg">
					<div class="title">Posljednji proizvodi</div>
				</div>
				<div id="uspjesno" style="display:none;">
				Radiiiiii
				</div>
				<div class="row prdct">
					<?php
						$sql1="SELECT * FROM artikli ORDER BY ID DESC LIMIT 15";
						$rez=$conn->query($sql1);
						while ($row = $rez->fetch_assoc()) {
							$id= $row["ID"];	
							$Naziv= $row["Naziv"];
							$Cijena= $row["Cijena"];							
							$Cijena2= $row["CijenaOld"];
							$slika= $row["Slika"];
							$Kategorija = $row["Kategorija"];
							if($Kategorija==1){
							$KategorijaN = 'Teretni program';
							}else if($Kategorija==2){
							$KategorijaN = 'Putnički program';
							}else if($Kategorija==3){
							$KategorijaN = 'Servisna uputstva';
							}else if($Kategorija==4){
							$KategorijaN = 'Viljuškari';
							}else if($Kategorija==5){
							$KategorijaN = 'Dijagnostički uređaji';
							}else{
							$KategorijaN = 'Nekategorisano';
							}
							if($Cijena2!=$Cijena){ $rasprodaja="on-sale";}else{$rasprodaja="";}
							?>							
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="productwrap">
									<div class="pr-img">
										<a class="hidden-xs" href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><img src="images/artikli/<?php echo $slika; ?>" alt="" class="img-responsive"/></a>
										<div class="pricetag <?php echo $rasprodaja; ?>"><div class="inner <?php echo $rasprodaja; ?>"><span class="onsale"><?php if($Cijena2!=$Cijena){?><span class="oldprice"><?php echo $Cijena2; ?> KM</span><?php }?><?php echo $Cijena; ?> KM</span></div></div>
									</div>
									<span class="smalltitle"><a href="product.php?ID=<?php echo $id; ?>&kat=<?php echo $Kategorija; ?>"><?php echo $Naziv ?></a></span>
									<span class="smalldesc"><?php echo $KategorijaN; ?></span><br/>
								<span class="smalldesc"><a href="#" class="add-to-cart" data-id="<?php echo $id ?>">Dodaj u predračun</a></span>
								</div>
							</div>
							<?php

						}
					
					?>
					</div>
				</div><!--Products-->
				<div class="spacer"></div>
			</div><!--Main content-->
		</div>
	</div>
	
	<div class="f-widget"><!--footer Widget-->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4"><!--footer newsletter widget-->

				</div><!--footer newsletter widget-->
				<div class="col-md-4"><!--footer contact widget-->
					<div class="title-widget-bg">
						<div class="title-widget-cursive">Kontakt</div>
					</div>
					<ul class="contact-widget">
						<li class="fphone">+387 123 456, +387 123 456 <br /> +387 123 456</li>
						<li class="fmobile">+387-123-456-1<br />+387-123-456-2</li>
						<li class="fmail lastone">your@email.com<br />customer.care@mail.com</li>
					</ul>
				</div><!--footer contact widget-->
			</div>
			<div class="spacer"></div>
		</div>
	</div><!--footer Widget-->
	
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- map -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
	<script type="text/javascript" src="js/jquery.ui.map.js"></script>
	<script type="text/javascript" src="js/demo.js"></script>
	
	<!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>
	
	<!-- rating -->
	<script src="js/rate/jquery.raty.js"></script>
	<script src="js/labs.js" type="text/javascript"></script>
	
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/product/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	
	<!-- fancybox -->
    <script type="text/javascript" src="js/product/jquery.fancybox.js=2.1.5"></script>
	
	<!-- custom js -->
    <script src="js/shop.js"></script>
	</div>
  </body>
</html>
